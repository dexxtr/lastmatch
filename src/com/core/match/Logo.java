package com.core.match;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import com.flurry.android.FlurryAgent;

public class Logo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		final Intent intent = new Intent(this, MainMenu.class);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		setContentView(R.layout.splash);

		final ScheduledExecutorService scheduler = Executors
				.newScheduledThreadPool(1);
		final Runnable delay = new Runnable() {
			public void run() {
				startActivity(intent);
				finish();
			}
		};
		@SuppressWarnings("unused")
		ScheduledFuture<?> delayHandle = scheduler.schedule(delay, 3,
				TimeUnit.SECONDS);
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "Y3U39EK78T5GQUS8KHUU");
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}