package com.core.match;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.WindowManager;

import com.flurry.android.FlurryAgent;

public class Prefs extends PreferenceActivity {

	private static final String OPT_MUSIC = "music";
	private static final boolean OPT_MUSIC_DEF = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		Music.pause();
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	public static boolean getMusic(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean(OPT_MUSIC, OPT_MUSIC_DEF);
	}

	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "Y3U39EK78T5GQUS8KHUU");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}