package com.core.match;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.Window;
import android.view.WindowManager;

import com.flurry.android.FlurryAgent;

public class HowToPlay extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.how_to_play);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Music.play(this, R.raw.main, false);
	}

	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "Y3U39EK78T5GQUS8KHUU");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
