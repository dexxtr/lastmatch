package com.core.match;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.ads.*;
import com.core.match.R.raw;
import com.flurry.android.FlurryAgent;

public class Game extends Activity {
	public int count = 18;
	private ArrayList<Match> matchList;
	public int selectedMatchesCount = 0;
	private LinearLayout imageLayout;
	private TextView countTextView;
	public TextView statusBarTextView;
	private int gameMode;
	private int currentPlayer = 1;
	private LinearLayout adLayout;

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    AdView adView = new AdView(this, AdSize.BANNER, "a14cbac94548cf2");

	    adLayout = new LinearLayout(this);
		adLayout.setOrientation(LinearLayout.HORIZONTAL);
		adLayout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		adLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		adLayout.addView(adView);
		
		adView.loadAd(new AdRequest());
		
		
		
		Bundle extras = getIntent().getExtras();
		gameMode = extras.getInt("GAME_MODE");

		LinearLayout parentLayout = new LinearLayout(this);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		parentLayout.setOrientation(LinearLayout.VERTICAL);
		parentLayout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		parentLayout.setBackgroundResource(R.drawable.game_background);

		parentLayout.setGravity(Gravity.CENTER);
		countTextView = new TextView(this);
		countTextView.setText("" + count);
		countTextView.setTextColor(Color.BLACK);
		countTextView.setTextSize(20f);
		countTextView.setGravity(Gravity.CENTER);
		LinearLayout.LayoutParams countTextViewLayoutParams = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		countTextViewLayoutParams.setMargins(10, 0, 0, 0);
		countTextView.setLayoutParams(countTextViewLayoutParams);

		ImageView countImageView = new ImageView(this);
		countImageView.setImageResource(R.drawable.matchpic);
		countImageView.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		ImageView takeButton = new ImageView(this);
		takeButton.setImageResource(R.drawable.takebutton);
		takeButton.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		takeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				playShortSound(Game.this, R.raw.take_them);
				switch (gameMode) {
				case 0:
					// SINGLEPLAYER
					if (selectedMatchesCount != 0) {
						if (count - selectedMatchesCount == 0) {
							statusBarTextView
									.setText("You should leave 1 match..");
						} else {
							for (Match match : matchList) {
								if (match.isHighlighted()) {
									match.setVisibility(View.GONE);
								}
							}
							count -= selectedMatchesCount;
							countTextView.setText("" + count);
							if (count < 12) {
								myTurn();
							} else {
								randomTurn();
							}
							countTextView.setText("" + count);
							selectedMatchesCount = 0;
						}
					} else {
						statusBarTextView.setText("Will you choose a match?");
					}
					break;
				case 1:
					// MULTIPLAYER
					if (selectedMatchesCount == count) {
						statusBarTextView.setText("You should leave 1 match..");
					} else if (selectedMatchesCount != 0) {
						for (Match match : matchList) {
							if (match.isHighlighted()) {
								match.setVisibility(View.GONE);
							}
						}
						count -= selectedMatchesCount;

						countTextView.setText("" + count);
						statusBarTextView.setText("Player " + getPlayer()
								+ " took " + selectedMatchesCount
								+ ", and Player " + getSecondPlayer() + "?");
						selectedMatchesCount = 0;
						if (count == 1) {
							showEndGameDialog("Player " + getPlayer() + " Won!");
						}
						switchPlayers();

					} else {
						statusBarTextView.setText("Will you choose a match?");
					}
					break;
				}

			}

			private void switchPlayers() {
				if (currentPlayer == 1) {
					currentPlayer = 2;
				} else {
					currentPlayer = 1;
				}
			}

			private int getPlayer() {
				return currentPlayer;
			}

			private int getSecondPlayer() {
				if (currentPlayer == 1) {
					return 2;
				} else
					return 1;
			}

		});

		imageLayout = new LinearLayout(this);
		LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		imageLayoutParams.setMargins(0, 15, 0, 10);
		imageLayout.setOrientation(LinearLayout.HORIZONTAL);
		imageLayout.setLayoutParams(imageLayoutParams);
		imageLayout.setGravity(Gravity.CENTER);

		matchList = new ArrayList<Match>();
		for (int i = 0; i < count; i++) {
			matchList.add(new Match(this, i));
		}

		for (Match match : matchList) {
			imageLayout.addView(match.getImage());
		}

		statusBarTextView = new TextView(this);
		statusBarTextView.setText(" ");
		statusBarTextView.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		statusBarTextView.setGravity(Gravity.CENTER_HORIZONTAL);
		statusBarTextView.setTextSize(20f);
		statusBarTextView.setTextColor(Color.BLACK);

		System.gc();

		LinearLayout statusLayout = new LinearLayout(this);
		statusLayout.setOrientation(LinearLayout.HORIZONTAL);
		statusLayout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		statusLayout.setGravity(Gravity.CENTER_HORIZONTAL);

		LinearLayout serviceLayout = new LinearLayout(this);
		serviceLayout.setOrientation(LinearLayout.HORIZONTAL);
		serviceLayout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		LinearLayout countLayout = new LinearLayout(this);
		countLayout.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams countLayoutParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
		countLayoutParams.setMargins(10, 0, 0, 5);
		countLayout.setLayoutParams(countLayoutParams);
		countLayout.setGravity(Gravity.BOTTOM | Gravity.LEFT);

		LinearLayout takeButtonLayout = new LinearLayout(this);
		takeButtonLayout.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		buttonLayoutParams.setMargins(0, 0, 5, 5);
		takeButtonLayout.setLayoutParams(buttonLayoutParams);
		takeButtonLayout.setGravity(Gravity.BOTTOM | Gravity.RIGHT);

		statusLayout.addView(statusBarTextView);

		countLayout.addView(countImageView);
		countLayout.addView(countTextView);

		serviceLayout.addView(countLayout);
		takeButtonLayout.addView(takeButton);
		serviceLayout.addView(takeButtonLayout);

		imageLayoutParams.setMargins(0, 15, 0, 10);
		parentLayout.addView(imageLayout);
		parentLayout.addView(statusLayout);
		parentLayout.addView(serviceLayout);

		this.setContentView(parentLayout);
		Music.play(this, raw.main, false); // TODO: Change it to game music

		// HERE THE GAME BEGINS

		switch (gameMode) {
		case 0:
			switch (new Random().nextInt(2)) {
			case 1:
				randomTurn();
				break;
			default:
				statusBarTextView.setText("Your turn, sir...");
				break;
			}
			break;
		case 1:
			statusBarTextView.setText("Player 1 begins...");
		}

	}

	private int getTurn(int old, int choosen) {
		int getback;
		switch ((old + choosen) % 4) {
		case 0:
			if (choosen == 1) {
				getback = 2;
			} else {
				getback = 1;
			}
			break;
		case 1:
			getback = 4 - choosen;
			break;
		case 2:
			if (choosen == 2) {
				getback = 3;
			} else {
				getback = 1;
			}
			break;
		default:
			if (choosen == 1) {
				getback = 1;
			} else {
				getback = 3;
			}
			break;
		}
		return getback;
	}

	private void randomTurn() {
		int turn = new Random().nextInt(3) + 1;
		count -= turn;
		countTextView.setText("" + count);
		for (int i = 0; i < turn; i++) {
			for (Match match : matchList) {
				if (match.image.getVisibility() != View.GONE) {
					match.setVisibility(View.GONE);
					break;
				}
			}
			statusBarTextView.setText("I took " + turn + ", and you ?");
		}
	}

	private void myTurn() {
		int turn;
		if (count == 1) {
			showEndGameDialog("You won!");
		} else {
			switch (count % 4) {
			case 1:
				turn = (int) (Math.random() * 3 + 1);
				break;
			default:
				turn = getTurn(count, selectedMatchesCount);
				break;
			}
			count -= turn;
			countTextView.setText("" + count);
			for (int i = 0; i < turn; i++) {
				for (Match match : matchList) {
					if (match.image.getVisibility() != View.GONE) {
						match.setVisibility(View.GONE);
						break;
					}
				}
			}
			if (count == 1) {
				showEndGameDialog("You've lost!");
			} else {
				statusBarTextView.setText("I took " + turn + ", and you ?");
			}
		}
	}

	private void showEndGameDialog(String action) {
		Music.stop();
		if (action.contains("Won!") || action.equalsIgnoreCase("You won!")) {
			playShortSound(this, R.raw.win);
		} else {
			playShortSound(this, R.raw.loose);
		}
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(action);
		alertDialog.setCancelable(false);
		alertDialog.setMessage("Would you like to play again?");
		alertDialog.setButton("New Game",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						playShortSound(Game.this, R.raw.click);
						alertDialog.dismiss();
						System.gc();
						openNewGameDialog();
					}
				});
		alertDialog.setButton2("Quit", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				playShortSound(Game.this, R.raw.click);
				finish();
				alertDialog.dismiss();
			}
		});

		alertDialog.show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// TODO: Insert Music.play(this, R.raw.game, true);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Music.stop();
	}

	private static void playShortSound(Context context, int resource) {
		MediaPlayer mp = MediaPlayer.create(context, resource);
		mp.start();
		mp.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
	}

	public void openNewGameDialog() {
		new AlertDialog.Builder(this)
				.setTitle("Game Mode")
				.setItems(R.array.game_mode,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								startGame(i);
							}
						}).show();
	}

	public void startGame(int i) {
		finish();
		Intent intent = new Intent(Game.this, Game.class);
		intent.putExtra("GAME_MODE", i);
		startActivity(intent);
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "Y3U39EK78T5GQUS8KHUU");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
