package com.core.match;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Match implements OnClickListener {

	public ImageView image;
	private final Game context;
	public boolean highlighted = false;

	public Match(Context context, int i) {
		this.context = (Game) context;
		image = new ImageView(context);
		image.setImageResource(R.drawable.game_match);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		image.setLayoutParams(layoutParams);
		image.setOnClickListener(this);
	}

	public ImageView getImage() {
		return image;
	}

	public void setImage(ImageView image) {
		this.image = image;
	}

	public void setVisibility(int visibility) {
		image.setVisibility(visibility);
	}

	@Override
	public void onClick(View v) {
		MediaPlayer mp = MediaPlayer.create(context, R.raw.click_on_match);
		if (mp != null) {
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		}

		if (highlighted == true) {
			highlighted = false;
			context.selectedMatchesCount--;
			image.setImageResource(R.drawable.game_match);
		} else if (context.selectedMatchesCount < 3) {
			highlighted = true;
			context.selectedMatchesCount++;
			if (context.getCurrentPlayer() == 1) {
				image.setImageResource(R.drawable.game_match_selected);
			} else {
				image.setImageResource(R.drawable.game_match_selected_blue);
			}
		} else {
			context.statusBarTextView.setText("You can't take more than 3!");
		}
	}

	public boolean isHighlighted() {
		return highlighted;
	}

}
