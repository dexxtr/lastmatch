package com.core.match;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.google.ads.*;
import com.core.match.R.raw;
import com.flurry.android.FlurryAgent;
import com.zigles.android.expll.Launcher;

public class MainMenu extends Activity implements OnClickListener {

	private static MediaPlayer mp;
	public static boolean IS_NETWORK_AVAILABLE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_menu);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		AdView adView = (AdView)this.findViewById(R.id.mainAd);
	    adView.loadAd(new AdRequest());
	    
		View newButton = findViewById(R.id.new_button);
		newButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
		View howToPlayButton = findViewById(R.id.how_to_play_button);
		howToPlayButton.setOnClickListener(this);
		View creditsButton = findViewById(R.id.poll_button);
		creditsButton.setOnClickListener(this);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			if (extras.getBoolean("SHOW_NEWGAME_DIALOG")) {
				finish();
				openNewGameDialog();
			}
		}
		Music.play(this, raw.main, true); // Music is interrupted

	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			Log.w("INTERNET", "couldn't get connectivity manager");
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settings_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Music.play(this, R.raw.click, false);
			startActivity(new Intent(this, Prefs.class));
			return true;
		case R.id.credits:
			startActivity(new Intent(this, Credits.class));
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		mp = MediaPlayer.create(MainMenu.this, R.raw.click);
		mp.start();
		mp.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
		switch (v.getId()) {
		case R.id.poll_button:
			Launcher exPllLauncher = new Launcher(this.getApplicationContext());
			exPllLauncher.run();
			break;
		case R.id.new_button:
			openNewGameDialog();
			break;
		case R.id.exit_button:
			finish();
			break;
		case R.id.how_to_play_button:
			startActivity(new Intent(this, HowToPlay.class));
			break;
		}
	}

	public void openNewGameDialog() {
		new AlertDialog.Builder(this)
				.setTitle("Game Mode")
				.setItems(R.array.game_mode,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								startGame(i);
							}
						}).show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Music.play(this, R.raw.main, false);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Music.stop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Music.stop();
		System.gc();
	}

	public void startGame(int i) {
		Intent intent = new Intent(MainMenu.this, Game.class);
		intent.putExtra("GAME_MODE", i);
		startActivity(intent);
	}
	
	public void onStart() {
 	   super.onStart();
 	   FlurryAgent.onStartSession(this, "Y3U39EK78T5GQUS8KHUU");
 	   
 	   String uid = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	   FlurryAgent.setUserId(uid);
	   
	   FlurryAgent.onPageView();
 	}
 	
 	public void onStop() {
 	   super.onStop();
 	   FlurryAgent.onEndSession(this);
 	}
}
