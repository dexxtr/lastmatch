package com.core.match;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {

	private static MediaPlayer mp = null;

	public static void play(Context context, int resource, boolean stopPlayback) {
		if (stopPlayback == true) {
			stop();
		}

		if (Prefs.getMusic(context)) {
			if (mp != null) {
				mp.start();
			} else {
				mp = MediaPlayer.create(context, resource);
				mp.setLooping(true);
				mp.start();
			}
		}
	}

	public static void stop() {
		if (mp != null) {
			mp.stop();
			mp.release();
			mp = null;
		}
	}

	public static void pause() {
		if (mp != null) {
			mp.pause();
		}
	}
}
